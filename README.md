# How to launch
## Setup
- install https://git-scm.com/
- from git console use ```git clone https://Virelion@bitbucket.org/Virelion/spring-rest-react.git```

## To launch server
### Setup
- make sure to have jdk8 java installed (best is 8u201 from https://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8-2177648.html?printOnly=1)
- make sure you have correctly setup JAVA_HOME and path to ```javac``` command

### To compile and run
Use ```./gradlew bootRun``` server starts on address ```http://localhost:8080```.
You can go to <http://localhost:8080/item> to see it at work.

## To launch frontend
### Setup
- install npm from https://www.npmjs.com/get-npm
- go to ```frontend``` directory
- type in console ```npm install``` to install packages

### Launch/recompile
Use ```webpack``` command and open ```index.html```


### Docker usage
To build type in docker console
```docker-compose build```

To run build image from command above and execute
```docker-compose up```