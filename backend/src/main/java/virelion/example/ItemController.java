package virelion.example;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
    private final AtomicInteger counter = new AtomicInteger();
    private final Random random = new Random(System.currentTimeMillis());
    private final String[] randomShit = new String[] {
            "Stary farmer farme miał",
            "Wtf is happening",
            "So much random",
            "Yeeet"
    };

    @CrossOrigin
    @RequestMapping("/item")
    public Item item() {
        return new Item(
                getRandomQuote(),
                counter.incrementAndGet()
        );
    }

    private String getRandomQuote() {
        return randomShit[Math.abs(random.nextInt())%randomShit.length];
    }
}
