import * as React from 'react'; // let's also import Component

type Response = {
  name: String,
  value: Number
}

class ExampleState {
  response: Response;
  
  constructor(response: Response) {
	this.response = response;
  }
}
// Clock has no properties, but the current state is of type ClockState
// The generic parameters in the Component typing allow to pass props
// and state. Since we don't have props, we pass an empty object.
export class ExampleRequestToRest extends React.Component<{}, ExampleState> {
  constructor(props: any) {
	super(props);
    this.state = new ExampleState({
	  name: "",
	  value: 0
	});
  }	  
	
  request() {
    fetch("http://localhost:8080/item")
	  .then(res => res.json() )
	  .then(res => this.setState(new ExampleState(res)));
  }

  // render will know everything!
  render() {
    return (
	<p>
	  <button onClick={this.request.bind(this)} > Click me </button>
	  <br />
	  {this.state.response.name}
	  <br />
	  {this.state.response.value}
	</p>);
  }
}