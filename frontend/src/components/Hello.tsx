import * as React from "react";
import {Clock} from './Clock';
import {ExampleRequestToRest} from './ExampleRequestToRest';

export interface HelloProps { compiler: string; framework: string; }

export const Hello = (props: HelloProps) => { 
	return (<div>
		<h1>Hello from {props.compiler} and {props.framework}!</h1>
		<Clock />
		<ExampleRequestToRest />
	</div>);
}